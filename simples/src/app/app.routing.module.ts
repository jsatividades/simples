import { EditarAlbumComponent } from './album/editar-album/editar-album.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaAlbumComponent } from './album/lista-album/lista-album.component';
import { ListaArtistaComponent } from './artista/lista-artista/lista-artista.component';
import { FormAlbumComponent } from './album/form-album/form-album.component';
import { FormArtistaComponent } from './artista/form-artista/form-artista.component';
import { DetalheAlbumComponent } from './album/detalhe-album/detalhe-album.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'lista',
    pathMatch: 'full',
    data: { title: 'Lista' }
  },
  {
    path: 'lista',
    component: ListaAlbumComponent,
    data: { title: 'Album' }
  },
  {
    path: 'artista',
    component: ListaArtistaComponent,
    data: { title: 'Artista' }
  },
  {
    path: 'novo',
    component: FormAlbumComponent,
    data: { title: 'Novo Album' }
  },
  {
    path: 'novoartista',
    component: FormArtistaComponent,
    data: { title: 'Novo Artista' }
  },
  {
    path: 'editar/:albumId',
    component: EditarAlbumComponent,
    data: { title: 'Editar' }
  },
  {
    path: 'detalhe/:albumId',
    component: DetalheAlbumComponent,
    data: { title: 'Detalhe' }
  },
  {
    path: '**',
    component: ListaAlbumComponent,
    data: { title: 'Not Found' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
