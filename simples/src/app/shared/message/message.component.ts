import { Component, Input } from '@angular/core';

@Component({
  selector: 'simples-message',
  templateUrl: 'message.component.html'
})
export class MessageComponent {
  @Input() mensagem: string = '';
}
