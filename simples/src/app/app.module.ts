import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { AlbumModule } from './album/album.module';
import { ArtistaModule } from './artista/artista.module';
import { AppRoutingModule } from './app.routing.module';
import { AlertModule } from './shared/alerts/alert.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AlbumModule,
    ArtistaModule,
    AppRoutingModule,
    AlertModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
