import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MessageModule } from './../../shared/message/message.module';
import { EditarAlbumComponent } from './editar-album.component';

@NgModule({
  declarations: [EditarAlbumComponent],
  exports: [EditarAlbumComponent],
  imports: [CommonModule, ReactiveFormsModule, MessageModule, RouterModule]
})
export class EditarAlbumModule {}
