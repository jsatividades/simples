import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Album } from './../album';
import { AlbumService } from './../album.service';
import { AlertService } from './../../shared/alerts/alert.service';
import { ArtistService } from '../../artista/artist.service';
import { Artist } from '../../artista/artist';

@Component({
  selector: 'simples-editar-album',
  templateUrl: './editar-album.component.html',
  styleUrls: ['./editar-album.component.css']
})
export class EditarAlbumComponent implements OnInit {
  editarAlbum: FormGroup;
  artists: Artist[] = [];
  album: Album;
  artist: Artist = new Artist();
  albumId: number;

  constructor(
    private formBuilder: FormBuilder,
    private albumService: AlbumService,
    private router: Router,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private artistService: ArtistService
  ) {}

  ngOnInit() {
    this.albumId = this.route.snapshot.params.albumId;
    if (!this.albumId) {
      this.router.navigate(['lista']);
    }

    this.retornaListaDeArtista();

    this.editarAlbum = this.formBuilder.group({
      Name: ['', Validators.required],
      Artist: ['', Validators.required]
    });

    this.albumService
      .buscarPorArtist(this.albumId)
      .subscribe(artista => (this.artist.Id = artista.Id));
    if (this.artist.Id) {
      this.alertService.info('Album sem artista relacionado');
    }
    this.albumService.buscarPorId(this.albumId).subscribe(dados => {
      this.editarAlbum.setValue({
        Name: dados.Name,
        Artist: this.artist.Id
      });
    });
  }

  private retornaListaDeArtista() {
    this.artistService
      .listar()
      .subscribe(dados => (this.artists = dados['value']));
  }

  update() {
    const name = this.editarAlbum.get('Name').value;
    const artist = this.editarAlbum.get('Artist').value;
    this.albumService.update(this.albumId, name, artist).subscribe(
      () => {
        this.alertService.success('Album atualizado com sucesso!');
        this.router.navigate(['lista']);
      },
      err => console.log(err)
    );
  }
}
