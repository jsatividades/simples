import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime} from 'rxjs/operators';
import { Router } from '@angular/router';

import { Album } from '../album';
import { AlbumService } from '../album.service';
import { AlertService } from './../../shared/alerts/alert.service';
import { Artist } from '../../artista/artist';

@Component({
  selector: 'simples-lista-album',
  templateUrl: './lista-album.component.html',
  styleUrls: ['./lista-album.component.css'],
  preserveWhitespaces: true
})
export class ListaAlbumComponent implements OnInit, OnDestroy {
  albuns$: Observable<Album[]>;
  albums: Album[] = [];
  artist: Artist;
  filter: string = '';
  debounce: Subject<string> = new Subject<string>();
  page: number = 1;
  total: number;

  constructor(
    private albumService: AlbumService,
    private router: Router,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.getAlbums();

    this.debounce
      .pipe(debounceTime(300))
      .subscribe(filter => (this.filter = filter));
  }
  private getAlbums() {
    this.albumService.listar().subscribe(dados => {
      this.albums = dados['value'];
    });
  }

  ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }

  excluir(albumId: number) {
    this.albumService.excluir(albumId).subscribe(
      () => {
        this.alertService.danger('Album foi removido com sucesso!');
        this.getAlbums();
      },
      err => console.log(err)
    );
  }

  getPage(page: number) {}
}
