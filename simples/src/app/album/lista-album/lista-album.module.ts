import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';

import { ListaAlbumComponent } from './lista-album.component';
import { FilterAlbum } from '../filter.album.pipe';

@NgModule({
  declarations: [
      ListaAlbumComponent, 
      FilterAlbum
    ],
  imports: [
      HttpClientModule, 
      CommonModule, 
      RouterModule,
      NgxPaginationModule
    ]
})
export class ListaAlbumModule {}
