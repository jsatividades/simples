import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { FormAlbumModule } from './form-album/form-album.module';
import { ListaAlbumModule } from './lista-album/lista-album.module';
import { DetalheAlbumModule } from './detalhe-album/detalhe-album.module';
import { EditarAlbumModule } from './editar-album/editar-album.module';

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    CommonModule,
    ListaAlbumModule,
    FormAlbumModule,
    DetalheAlbumModule,
    EditarAlbumModule
  ]
})
export class AlbumModule {}
