import { Artist } from '../artista/artist';
export class Album {
  Id: number;
  Name: string;
  Artist: Artist[];
}
