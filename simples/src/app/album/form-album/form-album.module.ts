import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { FormAlbumComponent } from './form-album.component';
import { MessageModule } from './../../shared/message/message.module';

@NgModule({
  declarations: [FormAlbumComponent],
  exports: [FormAlbumComponent],
  imports: [CommonModule, ReactiveFormsModule, MessageModule, RouterModule]
})
export class FormAlbumModule {}
