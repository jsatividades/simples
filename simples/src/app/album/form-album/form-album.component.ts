import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Album } from './../album';
import { AlbumService } from './../album.service';
import { AlertService } from './../../shared/alerts/alert.service';
import { ArtistService } from '../../artista/artist.service';
import { Artist } from '../../artista/artist';

@Component({
  selector: 'simples-form-album',
  templateUrl: './form-album.component.html',
  styleUrls: ['./form-album.component.css']
})
export class FormAlbumComponent implements OnInit {
  formAlbum: FormGroup;
  artists: Artist[] = [];
  album: Album;

  constructor(
    private formBuilder: FormBuilder,
    private albumService: AlbumService,
    private router: Router,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private artistService: ArtistService
  ) {}

  ngOnInit() {
    const albumId = this.route.snapshot.params.albumId;
    if (albumId) {
      this.albumService
        .buscarPorId(albumId)
        .subscribe(dados =>this.formAlbum.setValue(dados));
    }

    this.artistService
      .listar()
      .subscribe(dados => (this.artists = dados['value']));

    this.formAlbum = this.formBuilder.group({
      Name: ['', Validators.required],
      Artist: ['', Validators.required]
    });
  }

  salva() {
    const name = this.formAlbum.get('Name').value;
    const artist = this.formAlbum.get('Artist').value;

    this.albumService.salva(name, artist).subscribe(
      () => {
        this.alertService.success('Novo album adicionado com sucesso!');
        this.router.navigate(['lista']);
      },
      err => console.log(err)
    );
  }
}
