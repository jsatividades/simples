import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay } from 'rxjs/operators';

import { Album } from './album';
import { Artist } from '../artista/artist';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {
  private readonly API = `${environment.API}Album`;

  constructor(private http: HttpClient) {}

  listar() {
    return this.http.get<Album[]>(this.API).pipe(delay(1000));
  }

  listaPaginada(page: number) {
    const params = new HttpParams().append('$top', page.toString());
    return this.http.get<Album[]>(this.API, { params });
  }

  salva(name: string, artist: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let dados = {
      '@xdata.type': 'XData.Default.Album',
      'Name': name,
      'Artist@xdata.ref': 'Artist(' + artist + ')'
    };
    const album = JSON.stringify(dados);
    return this.http.post(this.API, album, { headers });
  }
  update(albumId: number,name: string, artist: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let dados = {
      '@xdata.type': 'XData.Default.Album',
      'Name': name,
      'Artist@xdata.ref': 'Artist(' + artist + ')'
    };
    const album = JSON.stringify(dados);
    return this.http.put(`${this.API}(${albumId})`, album, { headers });
  }

  buscarPorId(albumId: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    return this.http.get<Album>(`${this.API}(${albumId})`, { headers });
  }

  buscarPorArtist(albumId: number) {
    return this.http.get<Artist>(`${this.API}(${albumId})/Artist`);
  }

  excluir(albumId: number) {
    return this.http.delete(`${this.API}(${albumId})`);
  }
}
