import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Album } from './../album';
import { Artist } from '../../artista/artist';
import { AlbumService } from './../album.service';
import { AlertService } from './../../shared/alerts/alert.service';
import { ArtistService } from '../../artista/artist.service';

@Component({
  selector: 'simples-detalhe-album',
  templateUrl: './detalhe-album.component.html',
  styleUrls: ['./detalhe-album.component.css']
})
export class DetalheAlbumComponent implements OnInit {
  album$: Observable<Album>;
  album: Album;
  artist: Artist;

  constructor(
    private route: ActivatedRoute,
    private albumService: AlbumService,
    private router: Router,
    private alertService: AlertService,
    private artistService: ArtistService
  ) {}

  ngOnInit(): void {
    const albumId = this.route.snapshot.params.albumId;
    this.albumService.buscarPorId(albumId).subscribe(
      dados => {
        this.album = dados;
      },
      err => {
        this.alertService.info('Album não foi encontrado');
        this.router.navigate(['lista']);
      }
    );
    this.albumService
      .buscarPorArtist(albumId)
      .subscribe(dados => (this.artist = dados));
  }
}
