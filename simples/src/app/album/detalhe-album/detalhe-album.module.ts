import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DetalheAlbumComponent } from './detalhe-album.component';

@NgModule({
  declarations: [DetalheAlbumComponent],
  imports: [CommonModule]
})
export class DetalheAlbumModule {}
