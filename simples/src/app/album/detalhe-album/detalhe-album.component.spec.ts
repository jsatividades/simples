import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalheAlbumComponent } from './detalhe-album.component';

describe('DetalheAlbumComponent', () => {
  let component: DetalheAlbumComponent;
  let fixture: ComponentFixture<DetalheAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalheAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalheAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
