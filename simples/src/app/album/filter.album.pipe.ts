import { Pipe, PipeTransform } from '@angular/core';

import { Album } from './album';

@Pipe({
  name: 'filterAlbum'
})
export class FilterAlbum implements PipeTransform {
  transform(albuns: Album[], consulta: string) {
    consulta = consulta.trim().toLowerCase();
    if (consulta) {
      return albuns.filter(album =>
        album.Name.toLowerCase().includes(consulta)
      );
    } else {
      return albuns;
    }
  }
}
