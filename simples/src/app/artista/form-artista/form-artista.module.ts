import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MessageModule } from '../../shared/message/message.module';
import { FormArtistaComponent } from './form-artista.component';

@NgModule({
  declarations: [FormArtistaComponent],
  exports: [FormArtistaComponent],
  imports: [CommonModule, ReactiveFormsModule, MessageModule, RouterModule]
})
export class FormArtistaModule {}
