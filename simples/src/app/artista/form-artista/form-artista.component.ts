import { OnInit, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertService } from '../../shared/alerts/alert.service';
import { ArtistService } from './../artist.service';

@Component({
  selector: 'simples-form-artista',
  templateUrl: './form-artista.component.html',
  preserveWhitespaces: true
})
export class FormArtistaComponent implements OnInit {
  formArtist: FormGroup;

  constructor(
    private artistService: ArtistService,
    private formBuilder: FormBuilder,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.formArtist = this.formBuilder.group({
      Name: ['', Validators.required]
    });
  }

  salvar() {
    const name = this.formArtist.get('Name').value;
    this.artistService.salva(name).subscribe(
      () => {
        this.alertService.success('Novo artista adicionado com sucesso!');
        this.formArtist.reset();
      },
      err => console.log(err)
    );
  }
}
