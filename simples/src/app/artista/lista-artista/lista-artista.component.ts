import { OnInit, Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { ArtistService } from '../artist.service';
import { AlertService } from './../../shared/alerts/alert.service';
import { Artist } from '../artist';

@Component({
  selector: 'simples-lista-artista',
  templateUrl: './lista-artista.component.html'
})
export class ListaArtistaComponent implements OnInit, OnDestroy {
  artists: Artist[] = [];
  filter: string = '';
  debounce: Subject<string> = new Subject<string>();
  page: number = 1;
  total: number;

  constructor(
    private artistService: ArtistService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.getArtists();
    this.debounce
      .pipe(debounceTime(300))
      .subscribe(filter => (this.filter = filter));
  }
  ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }
  getArtists() {
    this.artistService.listar().subscribe(dados => {
      this.artists = dados['value'];
    });
  }
  excluir(artistaId: number) {
    this.artistService.excluir(artistaId).subscribe(
      () => {
        this.alertService.danger('Artista foi removido com sucesso!');
        this.getArtists();
      },
      err => console.log(err)
    );
  }
}
