import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';

import { ListaArtistaComponent } from './lista-artista.component';
import { FilterArtist } from './../filter.artista.pipe';

@NgModule({
  declarations: [ListaArtistaComponent, FilterArtist],
  exports: [ListaArtistaComponent],
  imports: [HttpClientModule, RouterModule, CommonModule, NgxPaginationModule]
})
export class ListaArtistaModule {}
