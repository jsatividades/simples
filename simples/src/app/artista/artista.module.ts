import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { ListaArtistaModule } from './lista-artista/lista-artista.module';
import { FormArtistaModule } from './form-artista/form-artista.module';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    ListaArtistaModule,
    FormArtistaModule
  ]
})
export class ArtistaModule {}
