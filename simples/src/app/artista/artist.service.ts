import { environment } from '../../environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { delay } from 'rxjs/operators';

import { Artist } from './artist';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {
  private readonly API = `${environment.API}Artist`;

  constructor(private http: HttpClient) {}

  listar() {
    return this.http.get<Artist[]>(this.API).pipe(delay(1000));
  }

  listaPaginada(page: number) {
    const params = new HttpParams().append('$top', page.toString());
    return this.http.get<Artist[]>(this.API, { params });
  }

  salva(name: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8'
    });
    let dados = {
      '@xdata.type': 'XData.Default.Artist',
      Name: name
    };
    const artista = JSON.stringify(dados);
    return this.http.post(this.API, artista, { headers });
  }

  excluir(artistId: number) {
    return this.http.delete(`${this.API}(${artistId})`);
  }
}
