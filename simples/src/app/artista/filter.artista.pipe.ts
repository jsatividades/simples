import { Pipe, PipeTransform } from '@angular/core';

import { Artist } from './artist';

@Pipe({
  name: 'filterArtist'
})
export class FilterArtist implements PipeTransform {
  transform(artists: Artist[], consulta: string) {
    consulta = consulta.trim().toLowerCase();
    if (consulta) {
      return artists.filter(artist =>
        artist.Name.toLowerCase().includes(consulta)
      );
    } else {
      return artists;
    }
  }
}
